# Zabbix_BCMStats

A Zabbix template to monitor Raspberry Pi's via SSH using the bcmstat utility. The bcmstat utility can be found at https://github.com/MilhouseVH/bcmstat

Word of caution; I've only tested this with Zabbix 4.4 and Raspberry Pi 4's.

# To use
There are a few things you need to use this template.

1) You MUST MUST have version v0.5.4 or later. This is critical for the J option that allows an exit after n updates. If your Raspberry Pi has an old version and you can't update, you can pull the latest copy and point the Zabbix template to your new location in Template App bcmstat -> items -> bcmstat -> Executed Script.

2) You need to have SSH configured for Zabbix. The [official documentation](https://www.zabbix.com/documentation/current/manual/config/items/itemtypes/ssh_checks) I found to be fairly straight forward. If this isn't working, then this template won't work either.

3) Zabbix should have come with the offical "Template App SSH Service", if you don't have that you will have to pull that from Zabbix Git repo.

# How does it work?
The item 'bcmstat' ssh's into the RPi and runs the bcmstat utility. The options that it uses are:
1) J3 which exits bcmstat after three iterations. I found that the first two seemed to always give odd results probably because of various buffers. Thus, 3 was the shortest that I could check.
2) m just says not to colorize. Doubt this makes a big difference, I just didn't want to risk odd characters possibly coming through.
3) H0 strips the headers.
4) Y doesn't show the under voltage.
5) q suppresses the configuration information.
6) p shows individual core loads.
7) r shows CPU load and memory usage.
8) g shows GPU memory.

Then I strip out the last two lines with tail which is the line I want and the footer. Then head strips the footer. It checks every 8 seconds as that is the best balance for my setup. Anything less than 5 had issues on my network, possibly because it's 3 seconds + SSH overhead. However, 5 seconds was causing too much load on my Zabbix instance.

The template doesn't keep the information. Once the dependent items run the data is gone. I strip out most of the information that is of use for the RPi4, but I realize that this could be problematic on other RPi's (0/2/3/ect). One future cleanup would be to do a discovery check or something to dtermine the number of cores. If you have a different RPi you might want to just delete/disable those items and you may need to adjust the two memory checks (GPU and CPU) to be the right field.

To adjust the the memory checks, go to the item for the check, then Preprocessing. It's just simple Java script that filters out empty blanks, then shoves into an array. Then it just parses that field.
